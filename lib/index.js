'use strict';

// Module.exports = {myfunction};

var esprima = require('esprima');
var recast = require('recast');
var fs = require('fs');

exports.parseAst = function(filePath){
	
	var contents = fs.readFileSync(__dirname + "/" + filePath, "utf8");
	
	var ast = esprima.parseScript(contents, {range:true, loc:true});
	
	return ast;
}

exports.recastAst = function(filePath, fileName){
	
	var contents = fs.readFileSync(__dirname + "/" + filePath, "utf8");
	
	var ast = recast.parse(contents, {sourceFileName: fileName});
	
	return ast;
}
