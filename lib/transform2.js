/**
 * 
 */

var recast = require('recast');

module.exports = function(fileInfo, api) {
	
	// parse file with recast parser
	var ast = recast.parse(fileInfo.source);
	
	// return the code property of rewritten ast 
	// jscodeshift writes the new ast to the file
	// there is also a .map property 
	return transform(ast).code;
}

function hasMember(list, item){
	
	for(var i = 0; i < list.length; i++){
		if (list[i] == item){
			return true;
		}
	}
	return false;
}

// apply a transformation to an ast
function transform(ast){
	
	var exportedFunctions = ['mul', 'sub'];
	
	// body ast node
	var body = ast.program.body;

	// builder object for various types of ast nodes
	var b = recast.types.builders;
	
	// it has several assert functions
	var n = recast.types.namedTypes;
	
	// add as first node in the body a new importDeclaration.
	// the declaration is constructed through appropriate parameters
	body.unshift(b.importDeclaration(
			[
				b.importSpecifier(b.identifier("sum")),
				b.importSpecifier(b.identifier("pi"))
			], 
			b.literal("lib/math")));
	
	// make all functions exported
	// visit all top level function declarations
	recast.visit(ast, {
		
		visitFunctionDeclaration: function(path){
			var fn = path.node;
			// log the ast node
//			console.log(fn);
			
			// export selected functions
			if (!hasMember(exportedFunctions, fn.id.name)){
				return false;
			}
				
			// construct a new exportNamedDeclaration node
			var exportedFunction = b.exportNamedDeclaration(fn);
			exportedFunction.comments = fn.comments; // copy comments from the function node
			delete fn.comments; // remove the comments from the function node
			
			// replace functio node with exportedFunction node
			path.replace(fn, exportedFunction);
			path.prune(fn); // should not be required,
			// but for some reason replace does not remove the function node...
			
			return false;
		}
	});
	
	// exports the new ast and keeps source mapping information (probably can be used
	// for tracking changes
	return recast.print(ast, {
		  sourceMapName: "map.json"});
}