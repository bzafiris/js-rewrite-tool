const assert = require('assert');
const jsRewriteTool = require('../index.js');
const recast = require('recast');

describe('parse with esprima', function() {
	it('parses a script', function() {

		var result = jsRewriteTool.parseAst('resources/script1.js');

		expect(result).not.toBeNull();

		// console.log(JSON.stringify(result, null, 4));

	});

});

describe('manipulate with recast', function() {
	it('manipulates a script', function() {

		var ast = jsRewriteTool.recastAst('resources/script1.js', 'script1.js');

		expect(ast).not.toBeNull();

		output = transform(ast);
		
		console.log(output.code);
		
		console.log(output.map);
		console.log(output);
		
		var SourceMapConsumer = require("source-map").SourceMapConsumer;
		
		var smc = new SourceMapConsumer(output.map);
		console.log(smc.originalPositionFor({
		  line: 1, column:15
		}));
		
		
	});
	
	function transform(ast){
		
		var add = ast.program.body[0];

		// Make sure it's a FunctionDeclaration (optional).
		var n = recast.types.namedTypes;
		n.FunctionDeclaration.assert(add);

		// If you choose to use recast.builders to construct new AST nodes, all
		// builder arguments will be dynamically type-checked against the Mozilla Parser
		// API.
		var b = recast.types.builders;

		// This kind of manipulation should seem familiar if you've used Esprima
		// or the Mozilla Parser API before.
		ast.program.body[0] = b.variableDeclaration("var", [ b
				.variableDeclarator(add.id, b.functionExpression(null,
						add.params, add.body)) ]);

		// Just for fun, because addition is commutative:
		add.params.push(add.params.shift());
		// console.log(JSON.stringify(result, null, 4));

		// give the source map name to generate sourcemap
		return recast.print(ast, {
			  sourceMapName: "script1-map.json"});
	}
	
});
