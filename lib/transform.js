/**
 * http://usejsdoc.org/
 */
var recast = require('recast');

module.exports = function(fileInfo, api){
	
	console.log(fileInfo);
	var ast = recast.parse(fileInfo.source);
	return transform(ast).code;
	
}


function transform(ast){
	
	var add = ast.program.body[0];

	// Make sure it's a FunctionDeclaration (optional).
	var n = recast.types.namedTypes;
	n.FunctionDeclaration.assert(add);

	// If you choose to use recast.builders to construct new AST nodes, all
	// builder arguments will be dynamically type-checked against the Mozilla Parser
	// API.
	var b = recast.types.builders;

	// This kind of manipulation should seem familiar if you've used Esprima
	// or the Mozilla Parser API before.
	ast.program.body[0] = b.variableDeclaration("var", [ b
			.variableDeclarator(add.id, b.functionExpression(null,
					add.params, add.body)) ]);

	// Just for fun, because addition is commutative:
	add.params.push(add.params.shift());
	// console.log(JSON.stringify(result, null, 4));

	// give the source map name to generate sourcemap
	return recast.print(ast, {
		  sourceMapName: "map.json"});
}