# js-rewrite-tool
> test ast rewritters

### Installation

Requires node and npm <https://www.npmjs.com/get-npm>

```sh
$ npm install --save js-rewrite-tool
```

```sh
$ npm install -g jscodeshift
```

### Run transformations

You can run jscodeshift to a directory, or individual files by providing a trasformation
description. The transformation can be created with the recast, jscodeshift or any other api.
The requirement is that the resulting AST to be compatible with that that the jscodeshift expects.
Probably needs to conform to the ESTREE/Esprima variant.

```sh
$ jscodeshift -t lib/transform2.js lib/resources
```



